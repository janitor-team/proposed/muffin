Source: muffin
Section: x11
Priority: optional
Maintainer: Debian Cinnamon Team <debian-cinnamon@lists.debian.org>
Uploaders:
 Maximiliano Curia <maxy@debian.org>,
 Margarita Manterola <marga@debian.org>,
 Fabio Fantoni <fantonifabio@tiscali.it>,
 Joshua Peisach <itzswirlz2020@outlook.com>,
 Norbert Preining <norbert@preining.info>,
 Christoph Martin <martin@uni-mainz.de>
Build-Depends:
        debhelper-compat (= 13),
        dh-sequence-gir,
        at-spi2-core <!nocheck>,
        adwaita-icon-theme <!nocheck>,
        cinnamon-desktop-data (>= 5.4),
        dbus <!nocheck>,
        dmz-cursor-theme <!nocheck>,
        gnome-pkg-tools (>= 0.10),
        cinnamon-settings-daemon (>= 5.4) <!nocheck>,
        gobject-introspection (>= 1.41.3),
        gtk-doc-tools (>= 1.15),
        libcairo2-dev (>= 1.10.0),
        libcanberra-gtk3-dev,
        libdrm-dev (>= 2.4.83) [linux-any],
        libegl-dev,
        libegl1-mesa-dev (>= 17) [linux-any],
        libfribidi-dev (>= 1.0.0),
        libgirepository1.0-dev (>= 0.9.12),
        libgl1-mesa-dev (>= 7.1~rc3-1~),
        libgles2-mesa-dev (>= 7.1~rc3-1~) | libgles2-dev,
        libglib2.0-dev (>= 2.61.1),
        libcinnamon-desktop-dev (>= 5.4),
        libgraphene-1.0-dev (>= 1.9.3),
        libgtk-3-dev (>= 3.19.8),
        libgudev-1.0-dev (>= 232) [linux-any],
        libice-dev,
        libinput-dev (>= 1.7) [linux-any],
        libjson-glib-dev (>= 0.13.2-1~),
        libpam0g-dev,
        libpango1.0-dev (>= 1.2.0),
        libpipewire-0.3-dev [linux-any],
        libsm-dev,
        libstartup-notification0-dev (>= 0.7),
        libsystemd-dev (>= 212) [linux-any],
        libwacom-dev (>= 0.13) [linux-any],
        libxau-dev,
        libx11-dev,
        libx11-xcb-dev,
        libxcb-randr0-dev,
        libxcb-res0-dev,
        libxcomposite-dev (>= 1:0.4),
        libxcursor-dev,
        libxdamage-dev,
        libxext-dev,
        libxfixes-dev,
        libxi-dev (>= 2:1.7.4),
        libxinerama-dev,
        libxkbcommon-dev (>= 0.4.3),
        libxkbcommon-x11-dev,
        libxkbfile-dev,
        libxrandr-dev,
        libxrender-dev,
        libxt-dev,
        meson (>= 0.50),
        pkg-config (>= 0.22),
        xauth <!nocheck>,
        xkb-data,
        xserver-xorg-core [linux-any],
        xvfb <!nocheck>,
        zenity
Standards-Version: 4.6.1
Rules-Requires-Root: no
Homepage: http://cinnamon.linuxmint.com/
Vcs-Browser: https://salsa.debian.org/cinnamon-team/muffin
Vcs-Git: https://salsa.debian.org/cinnamon-team/muffin.git

Package: gir1.2-meta-muffin-0.0
Section: introspection
Architecture: any
Multi-Arch: same
Depends: libmuffin0 (= ${binary:Version}),
         ${gir:Depends},
         ${misc:Depends},
         ${shlibs:Depends}
Description: GObject introspection data for Muffin
 Muffin is a window manager performing compositing as well based on
 GTK+ and Clutter and used in Cinnamon desktop environment.
 .
 This package contains the GObject introspection data which may be
 used to generate dynamic bindings.
Breaks: cinnamon (<< 5.4~), cinnamon-common (<< 5.4~)

Package: libmuffin0
Section: libs
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: adwaita-icon-theme,
         muffin-common (>= ${source:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Breaks: cinnamon (<< 5.4~), libmuffin-dev (<< 5.4.1-1~)
Replaces: libmuffin-dev (<< 5.4.1-1~)
Description: window and compositing manager (shared library)
 Muffin is a window manager performing compositing as well based on
 GTK+ and Clutter and used in Cinnamon desktop environment.
 .
 This package contains the shared library.

Package: muffin
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: adwaita-icon-theme,
         muffin-common (>= ${source:Version}),
         zenity,
         ${misc:Depends},
         ${shlibs:Depends}
Provides: x-window-manager
Recommends: cinnamon-session (>= 5.4~) | x-session-manager
Suggests: cinnamon-control-center (>= 5.4~), xdg-user-dirs
Replaces: muffin-doc
Description: window and compositing manager
 Muffin is a window manager performing compositing as well based on
 GTK+ and Clutter and used in Cinnamon desktop environment.
 .
 This package contains the core binaries.
Breaks: cinnamon (<< 5.4~), cinnamon-core (<< 5.4~)

Package: muffin-common
Section: misc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: window and compositing manager (data files)
 Muffin is a window manager performing compositing as well based on
 GTK+ and Clutter and used in Cinnamon desktop environment.
 .
 This package contains the shared files.

Package: libmuffin-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: gir1.2-meta-muffin-0.0 (= ${binary:Version}),
         libatk1.0-dev,
         libcairo2-dev,
         libcinnamon-desktop-dev (>= 5.4),
         libdrm-dev,
         libegl1-mesa-dev,
         libgdk-pixbuf2.0-dev,
         libgles2-mesa-dev (>= 7.1~rc3-1~) | libgles2-dev,
         libglib2.0-dev,
         libgraphene-1.0-dev (>= 1.9.3),
         libgtk-3-dev,
         libinput-dev (>= 1.7),
         libjson-glib-dev,
         libmuffin0 (= ${binary:Version}),
         libpango1.0-dev,
         libudev-dev,
         libx11-dev,
         libxcomposite-dev,
         libxdamage-dev,
         libxext-dev,
         libxfixes-dev,
         libxi-dev,
         libxrandr-dev,
         ${misc:Depends}
Breaks: libmuffin0 (<< 5.4.1-1~)
Replaces: libmuffin0 (<< 5.4.1-1~)
Description: window and compositing manager (development files)
 Muffin is a window manager performing compositing as well based on
 GTK+ and Clutter and used in Cinnamon desktop environment.
 .
 This package contains the development files.
